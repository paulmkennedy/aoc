.text
.align 2

.globl _start
_start:

.option push
.option norelax
    la gp, __global_pointer$
.option pop
    la      sp, _estack
    jal     main
    jal     exit

.globl syscall
syscall:
    la      t0, syscall_buffer
    sd      a0, (t0)
    sd      a1, 8(t0)
    sd      a2, 16(t0)
    sd      a3, 24(t0)
    sd      a4, 32(t0)
    sd      a5, 40(t0)
    sd      a6, 48(t0)
    sd      a7, 56(t0)

    la      t1, tohost
    sd      t0, (t1)

    la      t1, fromhost
1:  ld      t2, (t1)
    beqz    t2, 1b
    sd      zero, (t1)

    ld      a0, (t0)
    ret

# TODO: handle negative numbers
.globl print_number
print_number:

    addi sp, sp, -32
    sd ra, 24(sp)
    
    li      t0, 10
    mv      t2, sp
1:  rem     t1, a0, t0
    addi    t1, t1, 48
    sb      t1, 0(t2)
    addi    t2, t2, 1
    div     a0, a0, t0
    bgtz    a0, 1b

    li      t1, 10
    sb      t1, 0(t2)

    mv      a0, sp
    sub     a1, t2, a0
    jal     reverse_string

    mv      a0, sp
    sub     a1, t2, a0
    addi    a1, a1, 1
    jal     write_string

    ld      ra, 24(sp)
    addi    sp, sp, 32
    ret

# a0: char * s
# a1: int len
reverse_string:
    mv      t3, a0
    addi    a1, a1, -1
    add     a1, a1, t3
1:  lb      t0, 0(a0)
    lb      t1, 0(a1)
    sb      t0, 0(a1)
    sb      t1, 0(a0)
    addi    a0, a0, 1
    addi    a1, a1, -1
    blt     a0, a1, 1b
    ret

write_string:
    mv      a2, a0
    mv      a3, a1
    li      a0, 64
    li      a1, 1
    j       syscall

.globl exit
exit:
    mv      a1, a0
    li      a0, 93
    jal     syscall
1:  wfi
    j       1b

.section .data, "aw", @progbits
.align 3
.globl tohost
.globl fromhost
tohost:   .dword 0
fromhost: .dword 0

syscall_buffer: .skip 64
