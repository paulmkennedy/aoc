#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <inttypes.h>

typedef struct {
    char * data;
    size_t len;
    size_t cap;
} Buffer;

Buffer
read_file(const char * filename)
{
    FILE * fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    struct stat sb;
    if (fstat(fileno(fp), &sb) == -1) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    Buffer buffer = {
        .cap = sb.st_size+1,
        .len = sb.st_size,
        .data = malloc(sb.st_size+1)
    };
    fread(buffer.data, 1, buffer.len, fp);
    if (ferror(fp)) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    fclose(fp);
    buffer.data[buffer.len] = '\0';
    return buffer;
}
