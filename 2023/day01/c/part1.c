#include "../../common/common.c"

int main()
{
    Buffer input = read_file("../input.txt");
    char * tok;
    char * p = input.data;
    char * save = NULL;
    uint64_t sum = 0;
    while ((tok = strtok_r(p, "\n", &save))) {
        p = NULL;
        int first_digit = -1;
        int last_digit = -1;
        size_t tl = strlen(tok);
        for (size_t i = 0; i < tl; i++) {
            if (isdigit(tok[i])) {
                last_digit = tok[i] - '0';
                if (first_digit < 0) {
                    first_digit = last_digit;
                }
            }
        }
        sum += first_digit*10 + last_digit;
    }
    printf("%" PRId64 "\n", sum);
    return 0;
}
