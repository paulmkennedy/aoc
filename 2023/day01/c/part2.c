#include "../../common/common.c"

int main()
{
    Buffer input = read_file("../input.txt");
    char * tok;
    char * p = input.data;
    char * save = NULL;
    uint64_t sum = 0;
    while ((tok = strtok_r(p, "\n", &save))) {
        p = NULL;
        int first_digit = -1;
        int last_digit = -1;
        size_t tl = strlen(tok);
        for (size_t i = 0; i < tl; i++) {
            int digit = -1;

            if (isdigit(tok[i])) digit = tok[i] - '0';
            else if (strncmp(tok+i, "one",   3) == 0) digit = 1;
            else if (strncmp(tok+i, "two",   3) == 0) digit = 2;
            else if (strncmp(tok+i, "three", 5) == 0) digit = 3;
            else if (strncmp(tok+i, "four",  4) == 0) digit = 4;
            else if (strncmp(tok+i, "five",  4) == 0) digit = 5;
            else if (strncmp(tok+i, "six",   3) == 0) digit = 6;
            else if (strncmp(tok+i, "seven", 5) == 0) digit = 7;
            else if (strncmp(tok+i, "eight", 5) == 0) digit = 8;
            else if (strncmp(tok+i, "nine",  4) == 0) digit = 9;
            else if (strncmp(tok+i, "zero",  4) == 0) digit = 0;

            if (digit >= 0) {
                last_digit = digit;
                if (first_digit < 0)
                    first_digit = digit;
            }
        }
        sum += first_digit*10 + last_digit;
    }
    printf("%" PRId64 "\n", sum);
    return 0;
}
